use std::{fs, io};
use std::mem::size_of;
use std::ops::Index;

fn main() -> io::Result<()> {
    let logs_dir = "/home/andrii/_data/";

    let data = fs::read(format!("{}{}", logs_dir, "comments.log.2022-11-29.0")).unwrap();
    let data_str = std::str::from_utf8(data.as_slice()).unwrap();

    let lines = data_str.split("\n").collect::<Vec<&str>>();
    lines.iter()
        .filter(|l| l.contains("******** Getting"))
        .filter(|l| l.contains(".TOTAL =="))
        .filter(|l| l.contains("https://"))
        .for_each(|l| parse_logfile(l));

    Ok(())
}

fn parse_logfile(line: &str)  {

    #[derive(Debug)]
    struct StatModel {
        when: String,
        service: String,
        comm_type: String,
        host: String,
        page: String,
        space: String,
        count: *const i16,
    }

    let comm_type = &line[line.find("******** Getting").unwrap() + 17 .. line.find("comments for").unwrap()].trim();
    let when = &line[..23];
    let service = &line[line.find("8080-exec-").unwrap() + 13..line.find(": ********").unwrap()].trim();
    let host = &line[line.find("https://").unwrap() .. line.find(" for page ").unwrap()];
    let page = &line[line.find("for page ").unwrap() + 9 .. line.find(".TOTAL ==").unwrap()];
    let space = "";
    let count = &line[line.find(".TOTAL ==").unwrap() + 10 ..].parse::<i16>().unwrap();

    let mut stats_model = StatModel {
        when: when.to_string(),
        service: service.to_string(),
        comm_type: comm_type.to_string(),
        host: host.to_string(),
        page: page.to_string(),
        space: space.to_string(),
        count,
    };


    println!("{:?}", &stats_model);

}

fn read_all_logs() -> io::Result<()> {
    let logs_dir = "/home/andrii/_data";
    let mut entries = fs::read_dir(logs_dir).unwrap()
        .map(|res| res.map(|e| e.path()))
        .collect::<Result<Vec<_>, io::Error>>()?;

    entries.sort();
    entries.iter()
        .filter(|f| f.is_file())
        .for_each(|file| {
            let read_bytes = fs::read(file.file_name().unwrap().to_str().unwrap()).unwrap();
            println!("{:?}", read_bytes);
        });

    Ok(())
}
